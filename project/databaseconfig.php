<?php
function gen($n){
  $b = array(  'lorem', 'ipsum', 'dolor', 'sit', 'amet', 'consectetur', 'adipiscing', 'elit',
          'a', 'ac', 'accumsan', 'ad', 'aenean', 'aliquam', 'aliquet', 'ante',
          'aptent', 'arcu', 'at', 'auctor', 'augue', 'bibendum', 'blandit',
          'class', 'commodo', 'condimentum', 'congue', 'consequat', 'conubia',
          'convallis', 'cras', 'cubilia', 'curabitur', 'curae', 'cursus',
          'dapibus', 'diam', 'dictum', 'dictumst', 'dignissim', 'dis', 'donec',
          'dui', 'duis', 'efficitur', 'egestas', 'eget', 'eleifend', 'elementum',
          'enim', 'erat', 'eros', 'est', 'et', 'etiam', 'eu', 'euismod', 'ex',
          'facilisi', 'facilisis', 'fames', 'faucibus', 'felis', 'fermentum',
          'feugiat', 'finibus', 'fringilla', 'fusce', 'gravida', 'habitant',
          'habitasse', 'hac', 'hendrerit', 'himenaeos', 'iaculis', 'id',
          'imperdiet', 'in', 'inceptos', 'integer', 'interdum', 'justo',
          'lacinia', 'lacus', 'laoreet', 'lectus', 'leo', 'libero', 'ligula',
          'litora', 'lobortis', 'luctus', 'maecenas', 'magna', 'magnis',
          'malesuada', 'massa', 'mattis', 'mauris', 'maximus', 'metus', 'mi',
          'molestie', 'mollis', 'montes', 'morbi', 'mus', 'nam', 'nascetur',
          'natoque', 'nec', 'neque', 'netus', 'nibh', 'nisi', 'nisl', 'non',
          'nostra', 'nulla', 'nullam', 'nunc', 'odio', 'orci', 'ornare',
          'parturient', 'pellentesque', 'penatibus', 'per', 'pharetra',
          'phasellus', 'placerat', 'platea', 'porta', 'porttitor', 'posuere',
          'potenti', 'praesent', 'pretium', 'primis', 'proin', 'pulvinar',
          'purus', 'quam', 'quis', 'quisque', 'rhoncus', 'ridiculus', 'risus',
          'rutrum', 'sagittis', 'sapien', 'scelerisque', 'sed', 'sem', 'semper',
          'senectus', 'sociosqu', 'sodales', 'sollicitudin', 'suscipit',
          'suspendisse', 'taciti', 'tellus', 'tempor', 'tempus', 'tincidunt',
          'torquent', 'tortor', 'tristique', 'turpis', 'ullamcorper', 'ultrices',
          'ultricies', 'urna', 'ut', 'varius', 'vehicula', 'vel', 'velit',
          'venenatis', 'vestibulum', 'vitae', 'vivamus', 'viverra', 'volutpat',
          'vulputate',
        );     
  $rand = array(); 
  shuffle($b);
  for ($i = 0; $i < $n; $i++) {
    $rand[$i] = $b[$i];
  }
  $rand = implode(" ", $rand);
  return $rand;
}

function db_get_connection() {
  require "config.php";
  static $db;
  try {
    if (!isset($db)) {
      $db = new PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } 
  } catch(PDOException $ex) {
      echo "Connection failed! " . $ex->getMessage();
  }
  return $db;
}

function add_tables($dbname, $username, $password, $dummyno) {
  try {
    $conn1 = new PDO("mysql:host=$host", $username, $password);
    $conn1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "DROP DATABASE IF EXISTS " . $dbname . ";";
    $conn1->exec($sql);
    $db = "CREATE DATABASE " . $dbname;
    $usedb = "USE " . $dbname; 
    $conn1->exec($db);
    $conn1->exec($usedb);
    $table1 = "CREATE TABLE blog (
              bid INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              title TEXT NOT NULL,
              content TEXT NOT NULL,
              date DATE NOT NULL)";    
    $conn1->exec($table1);    
    $table2 = "CREATE TABLE tag (
              tid INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              tags varchar(100) NOT NULL UNIQUE)";    
    $conn1->exec($table2);
    $table3 = "CREATE TABLE category (
      cid INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
      categories varchar(100) NOT NULL UNIQUE)";    
    $conn1->exec($table3);
    $table4 = "CREATE TABLE relation (
               blogid INT(11) NOT NULL,
              tagid INT(11) NOT NULL,
              categoryid INT(11) NOT NULL,
              FOREIGN KEY (blogid) REFERENCES blog (bid),
              FOREIGN KEY (tagid) REFERENCES tag (tid),
              FOREIGN KEY (categoryid) REFERENCES category (cid))";
    $conn1->exec($table4);      
    if (isset($_POST['dummyno'])) {
      for ($k = 0; $k < number_format($dummyno); $k++) {
        $dummycurrentdate = date("Y-m-d H:i:s");
        $dummyt = gen(4);
        $dummyb = gen(100);
        $dummyta = gen(2);
        $dummyc = gen(2);
        //$res = add_post($conn1, $dummyt, $dummyb, $dummyta, $dummycurrentdate, $dummyc);
        $words = explode(" ", $dummyta);
        $s = sizeof($words);  
        for($i = 0; $i < $s; $i++) {
          $words[$i] = trim($words[$i]);
        }
        $words2 = explode(" ", $dummyc);
        $s2 = sizeof($words2);  
        for($i = 0; $i < $s2; $i++) {
          $words2[$i] = trim($words2[$i]);
        }
        $sql = "INSERT INTO blog (title, content, date) VALUES ('$dummyt', '$dummyb', '$dummycurrentdate')";
        $conn1->exec($sql);  
        for ($i = 0; $i < $s; $i++) {
          $resl = $conn1->query("SELECT  tags FROM tag  WHERE  tags = '$words[$i]'");      
          $count = $resl->rowCount();                  
          if ($count == 0) {
            $tagsq="INSERT INTO tag (tags)  VALUES('$words[$i]')";
            $conn1->exec($tagsq);
          }
          $resl2 = $conn1->query("SELECT  categories FROM category  WHERE  categories = '$words2[$i]'");      
          $count2 = $resl2->rowCount();                  
          if ($count2 == 0) {
            $tagsq2="INSERT INTO category (categories)  VALUES('$words2[$i]')";
            $conn1->exec($tagsq2);
          }
          $relquery = "INSERT INTO relation (blogid, tagid, categoryid)
                        SELECT bd.bid, tt.tid, cc.cid FROM blog bd 
                        JOIN	tag tt ON bd.title = '$dummyt' AND tt.tags = '$words[$i]'
                        JOIN category cc ON bd.title = '$dummyt' AND cc.categories = '$words2[$i]'"; 
          $conn1->exec($relquery);
        }
      }
    }
  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
  }
}

function add_post($conn, $title, $blogg, $tag, $date, $catar)	{
  $tag = str_replace(" ", '',$tag);
  $tag = rtrim($tag, ",");
  $tag = strtolower($tag);
  $words = explode(",", $tag);
  $s = sizeof($words);
  for ($i = 0; $i < $s; $i++) {
    $words[$i] = trim($words[$i]);
  }
  $s2 = count($catar);
  try { 
    $blogg = addslashes($blogg);
    $sql = "INSERT INTO blog (title, content, date) VALUES ('$title', '$blogg', '$date')";
    $conn->exec($sql);
    for ($i =0; $i < $s; $i++) {
      $resl = $conn->query("SELECT  tags FROM tag  WHERE  tags = '$words[$i]'");
      $count = $resl->rowCount();
      if ($count == 0) {
        $tagsq = "INSERT INTO tag (tags)  VALUES ('$words[$i]')";
        $conn->exec($tagsq);
      }
    }
    for ($i =0; $i < $s; $i++) {
      for ($j =0; $j < $s2; $j++) {
      $resl2 = $conn->query("SELECT  categories FROM category  WHERE  categories = '$catar[$j]'");      
      $count2 = $resl2->rowCount();                  
      if ($count2 == 0) {
        $tagsq2="INSERT INTO category (categories)  VALUES('$catar[$j]')";
        $conn->exec($tagsq2);
      }
      $relquery = "INSERT INTO relation (blogid, tagid, categoryid)
                  SELECT bd.bid, tt.tid, cc.cid
                  FROM blog bd JOIN	tag tt
                  ON bd.title = '$title' AND tt.tags = '$words[$i]'
                  JOIN category cc ON bd.title = '$title' AND cc.categories = '$catar[$j]'";
      $conn->exec($relquery);
    }
    }
    return TRUE;
  } catch(PDOException $e) {
      echo "Connection failed: " . $e->getMessage();
  }
}

function category_display($conn)	{
  $stmt = $conn->prepare("SELECT DISTINCT cid, categories FROM category ORDER BY cid");                 
  $stmt->execute();
  $data = $stmt->fetchAll();
  return $data;	
}

function category_selected($conn , $idval, $cid)	{
  $sql = "SELECT blogid FROM relation WHERE blogid = '$idval' AND categoryid = '$cid';";
  $res = $conn->query($sql);
  $res->setFetchMode(PDO::FETCH_ASSOC);
  return $res;	
}

function category_display_single($conn, $idval)	{
  $stmt = $conn->prepare("SELECT categories FROM category WHERE cid = '$idval'");                 
  $stmt->execute();
  $data = $stmt->fetch();
  return $data;	
}

function category_update($conn, $catg, $idval)	{
  $stmt = $conn->prepare("UPDATE category SET categories = '$catg' WHERE cid = $idval");                 
  $stmt->execute();
  return TRUE;	
}

function category_add($conn, $category)	{
  $sql = "INSERT INTO category (categories) VALUES ('$category')";                 
  $conn->exec($sql);
}

function update_post($conn, $title, $blogg, $tag, $idval, $catar) {
  $tag = str_replace(" ", '',$tag);
  $tag = rtrim($tag, ",");
	$tag = strtolower($tag);
	$words = explode(",", $tag);
	$s = sizeof($words);
	for ($i = 0; $i < $s; $i++) {
    $words[$i] = trim($words[$i]);
  }
  $s2 = count($catar);
	try {  
    $blogg = addslashes($blogg);
    $sql = "UPDATE blog SET title = '$title', content = '$blogg' WHERE bid = $idval";
    $conn->exec($sql);
    $del = "DELETE FROM relation where blogid = $idval";
    $conn->exec($del);
    for ($i =0; $i < $s; $i++) {
      $resl = $conn->query("SELECT  tags FROM tag  WHERE  tags = '$words[$i]'");
      $count = $resl->rowCount();
      if ($count == 0) {
        $tagsq = "INSERT INTO tag (tags)  VALUES ('$words[$i]')";
        $conn->exec($tagsq);
      }
    }
    for ($i =0; $i < $s; $i++) {
      for ($j =0; $j < $s2; $j++) {
      $resl2 = $conn->query("SELECT  categories FROM category  WHERE  categories = '$catar[$j]'");      
      $count2 = $resl2->rowCount();                  
      if ($count2 == 0) {
        $tagsq2="INSERT INTO category (categories)  VALUES('$catar[$j]')";
        $conn->exec($tagsq2);
      }
      $relquery = "INSERT INTO relation (blogid, tagid, categoryid)
                  SELECT bd.bid, tt.tid, cc.cid
                  FROM blog bd JOIN	tag tt
                  ON bd.title = '$title' AND tt.tags = '$words[$i]'
                  JOIN category cc ON bd.title = '$title' AND cc.categories = '$catar[$j]'";
      $conn->exec($relquery);
    }
    }
    return TRUE;  
  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
  }
}

function post_displayer($conn, $id)	{
$stmt = $conn->prepare("SELECT  title , content, date FROM  blog WHERE bid=?");
$stmt->execute([$id]); 
return 	$stmt->fetch();
}

function all_post_displayer($conn, $offset, $n, $sort)	{
  $stmt = $conn->prepare("SELECT bid, title, content, date FROM blog 
  ORDER BY bid $sort LIMIT $offset, $n");                 
  $stmt->execute();
  $data = $stmt->fetchAll();
  return $data;	
}

function related_post_displayer($conn, $offset, $n, $sort, $tagidvalue2)	{
  $sql = "SELECT DISTINCT bid, title, content, date FROM blog, relation 
        WHERE blog.bid = relation.blogid and $tagidvalue2 = relation.tagid 
        ORDER BY bid $sort LIMIT $offset, $n";                   
  if (filter_var($tagidvalue2, FILTER_VALIDATE_INT)) {
    $stmt = $conn->prepare($sql); 
    $stmt->execute();
    $data = $stmt->fetchAll();
  }
  return $data;
}

function category_post_displayer($conn, $offset, $n, $sort, $catidvalue2)	{
  $sql = "SELECT DISTINCT bid, title, content, date FROM blog, relation 
        WHERE blog.bid = relation.blogid and $catidvalue2 = relation.categoryid 
        ORDER BY bid $sort LIMIT $offset, $n";                   
  if (filter_var($catidvalue2, FILTER_VALIDATE_INT)) {
    $stmt = $conn->prepare($sql); 
    $stmt->execute();
    $data = $stmt->fetchAll();
  }
  return $data;
}

function tag_displayer($idval, $conn)	{
  $sql1 = "SELECT DISTINCT tag.tags, tag.tid FROM relation, tag 
          WHERE relation.blogid = ? AND tag.tid = relation.tagid";
  $stmt2 = $conn->prepare($sql1);
  $stmt2->execute([$idval]);
  return $stmt2->fetchAll();
}

function category_displayer($idval, $conn)	{
  $sql1 = "SELECT DISTINCT category.cid, category.categories FROM relation, category 
          WHERE relation.blogid = ? AND category.cid = relation.categoryid";
  $stmt2 = $conn->prepare($sql1);
  $stmt2->execute([$idval]);
  return $stmt2->fetchAll();
}

function category_delete($conn, $cid) {
  try { 
    $del = "DELETE  FROM category  WHERE  cid = $cid";
    $conn->exec($del);
    return TRUE;  
  } catch(PDOException $e) {
    echo "Connection failed: Cannot be deleted as certain posts are related to this category!!!";
  }
}

function page_counter($n, $conn)	{
  $total_pages_sql = "SELECT bid FROM blog";
  $q1 = $conn->query($total_pages_sql);
  $total_rows = $q1->rowCount();
  $total_pages = ceil($total_rows / $n);
  return $total_pages;
}

function page_counter_relpost($n, $conn, $tagidvalue2)	{
  $total_pages_sql = "SELECT DISTINCT bid, title, content, date FROM blog, relation 
  WHERE blog.bid = relation.blogid and $tagidvalue2 = relation.tagid";
  $q1 = $conn->query($total_pages_sql);
  $total_rows = $q1->rowCount();
  $total_pages = ceil($total_rows / $n);
  return $total_pages;
}

function page_counter_catpost($n, $conn, $catidvalue2)	{
  $total_pages_sql = "SELECT DISTINCT bid, title, content, date FROM blog, relation 
  WHERE blog.bid = relation.blogid and $catidvalue2 = relation.categoryid";
  $q1 = $conn->query($total_pages_sql);
  $total_rows = $q1->rowCount();
  $total_pages = ceil($total_rows / $n);
  return $total_pages;
}

function content_trimmer($str)	{
  $words = explode(" ", $str);
  $cont =  implode(" ", array_splice($words, 0, 200));
  if (str_word_count($cont) > 199) {
    $cont= $cont."...";
  }
  return $cont;
}

function  delete_relation($idval, $conn) {
  $del="DELETE from relation where blogid = $idval";
  $conn->exec($del);
  return TRUE;
}

function delete_blog($idval, $conn) {
  $del = "DELETE  FROM blog  WHERE  bid = $idval";
  $conn->exec($del);
}
?>