<?php
require "databaseconfig.php";
$conn = db_get_connection();
if (isset($_GET["did"])) {
	$idval = $_GET["did"];
	$stat3 = delete_relation($idval, $conn);
	if (isset($stat3)) {
		delete_blog($idval, $conn); 
  	header("location:index.php?status3=1");
	}
	
}
if (isset($_GET["cid"])) {
    $cid = $_GET["cid"];
		$stat2 = category_delete($conn, $cid);
		if (isset($stat2)) {
			header("location:category.php?status2=1");
		}
}

?>