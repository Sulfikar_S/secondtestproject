<?php
require 'databaseconfig.php';
$conn = db_get_connection();
$idval = $_GET["eid"];
$catdata = category_display($conn);
if(filter_var($idval, FILTER_VALIDATE_INT)){
  $row = post_displayer($conn, $idval);
}
$data2 = tag_displayer($idval, $conn);                 
if (isset($_POST['addblog'])) {
  if (isset($_POST['title'])) {
  	$title = $_POST['title'];
  }
	if (isset($_POST['blog'])) {
  	$blogg = $_POST['blog'];
  }
  if (isset($_POST['tags'])) {
  	$tag = $_POST['tags'];
  }
  if(isset($_POST['catarray'])) {
    $catar = $_POST['catarray'];
  }
  $statusedit = update_post($conn, $title, $blogg, $tag, $idval, $catar);
  if (isset($statusedit)) {
    header("location:post.php?id=$idval&status=1");
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Test Post</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="addblog.php">Add Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="category.php">Categories</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/blog-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page-heading">
            <h1>Edit your blog</h1>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <p>Fill out your Name and text on the below form.</p>
        
        <form name="blogform" method="POST">
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Title</label>
              <input type="text" class="form-control" value="<?php echo $row["title"]; ?>" name="title" required data-validation-required-message="Please enter your name.">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Blog</label>
              <textarea rows="10" class="form-control" name="blog" required data-validation-required-message="Please type your content."><?php echo $row["content"]; ?></textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Tags</label>
              <textarea rows="3" class="form-control" placeholder="Tags" name="tags" required data-validation-required-message="Please type your content."><?php 
              if (isset($data2)) { 
                foreach ($data2 as $row2) {
                	echo $row2["tags"] . ",";        
                }
              }
              ?>
            </textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <select id="selectcategory" name='catarray[]' multiple="multiple" size=6>
         <option value="" disabled>--Select Category--</option>
          <?php foreach($catdata as $row){
          $cid = $row['cid'];
          $res = category_selected($conn , $idval, $cid);
          $count = $res->rowCount();
          echo '<label for="one"> ';
          if ($count == 0) {
             echo '<option value="'.$row["categories"].'"  >'.$row["categories"].'</option>';
          } else {               
            echo '<option value="'.$row["categories"].'"  selected="selected">'.$row["categories"].'</option>';
          }
        }
          ?>                
          </select>
          <br>
          <div id="success"></div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name= "addblog" id="AddBlogButton">Add</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>
